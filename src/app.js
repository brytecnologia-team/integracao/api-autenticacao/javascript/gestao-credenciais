const cron = require("node-cron");
const monitor = require("./service/monitor")
const controller = require("./controller/auth")
const express = require('express');
require('dotenv').config();

console.log("Starting...");

/*
    Update execution interval.
    Configured to be executed every X minutes.
*/
let cronExpression = '*/' + process.env.IDP_ADDRESS + ' * * * *';
cron.schedule(cronExpression, function() {
    monitor.monitor();
}, {});

app = express();
app.get("/jwt", function(req, res) {
    controller.getAccessToken(req, res)
});

app.listen(3333, () => {
    console.log('Server started on port 3333!');
});



