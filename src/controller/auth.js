const cache = require('memory-cache');
const {mapToJSON} = require("./dto/mapper");

function getAccessToken(req, res) {
    if(cache.get('currentCredential') == null) {
        console.log("Not a valid credential available.")
        res.status(500).contentType("application/json").json("Not a valid credential available.");
    }else{
        console.log("Credential valid.")
        let jsonBody = mapToJSON(cache);
        res.status(200).contentType("application/json").json(jsonBody);
    }
}

module.exports = {
    getAccessToken: getAccessToken,
};
