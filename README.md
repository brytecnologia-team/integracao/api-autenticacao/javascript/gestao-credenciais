# Gestão de validade da credencial de acesso

Este projeto apresenta uma abordagem de gerenciamento do tempo de vida das credenciais emitidas pela plataforma da BRy ([BRy Cloud]).

A abordagem apresenta um gerenciamento ativo, onde uma tarefa realiza validações períodicas do período de vigência do access token armazenado em memória.

O access token armazenado em memória válido e monitorado é disponibilizado na forma de serviço através do endereço: http://localhost:3333/jwt

Abaixo seguem os passos principais:
* Ao iniciar, a aplicação exemplo requisita um **Access Token** para a plataforma de serviços da BRy, utilizando as credenciais de uma aplicação cadastrada: **client_id** e **client_secret**.  
* Em um período configurável, a tarefa desperta e realiza a validação do tempo de vida do Access Token (processo de decodifição do JWT e extração da claim **exp**).
* Sempre que o Access Token estiver próximo de sua expiração, a tarefa dispara o evento de renovação da credencial utilizando o **Refresh Token**.

### Tech

O exemplo utiliza a biblioteca JavaScript abaixo:
* [Axios] - Promise based HTTP client for the browser and node.js
* [Dotenv] - Dotenv is a zero-dependency module that loads environment variables from a .env file into process.env
* [Express] - Fast, unopinionated, minimalist web framework for node
* [Jwt-decode] - Library for decoding JWT token.
* [Node Cron] - The node-cron module is tiny task scheduler for Javascript.
* [Memory Cache] - A simple in-memory cache for node.js.

### Variáveis que devem ser configuradas

O exemplo necessita ser configurado com os dados de conexão e identificação da aplicação cliente.

| Variável | Descrição | Arquivo |
| ------ | ------ | ------ |
| IDP_ADDRESS | Endereço do endpoint de obtenção de credenciais. | .env
| IDP_CLIENT_ID | **client_id** de uma aplicação. | .env
| IDP_CLIENT_SECRET | **client_secret** de uma aplicação. | .env
| IDP_TASK_UPDATE_INTERVAL_IN_MINUTES | Intervalo em minutos da execução da tarefa de monitoramento. | .env

### Observações

Os atributos de data e hora codificados nos tokens JWT (access token e refresh token), seguem o fuso horário UTC-0.
Lembrando que o Brasil possui 4 fusos horários:
* UTC-2 - Atol das Rocas e Fernando de Noronha.
* UTC-3 **(oficial)** - Estados não citados nos demais itens.
* UTC-4 - Amazonas, Mato Grosso, Mato Grosso do Sul, Rondônia e Roraima.
* UTC-5 - Acre e alguns municípios do Amazonas.


### Uso

Na pasta raiz do projeto, execute o comando de instalação conforme o gerenciador de dependências de sua preferência:

Instalando as dependências com npm:

    npm install

Para executar a aplicação execute o comando:

    npm run start
 
 [BRy Cloud]: <https://cloud.bry.com.br>
 [Axios]: <https://github.com/axios/axios>
 [Dotenv]: <https://github.com/motdotla/dotenv>
 [Express]: <https://github.com/expressjs/express>
 [Jwt-decode]: <https://github.com/auth0/jwt-decode>
 [Node Cron]: <https://github.com/node-cron/node-cron>
 [Memory Cache]: <https://github.com/ptarjan/node-cache>
 
